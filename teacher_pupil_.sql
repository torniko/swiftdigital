USE [master]
GO

CREATE DATABASE [Demo]
GO
USE [Demo]
GO

CREATE TABLE Teacher(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [varchar](6) NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Teacher] PRIMARY KEY (Id)
 )
 GO

 CREATE TABLE Pupil(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FIrstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [varchar](6) NOT NULL,
	[Class] [int] NOT NULL,
	CONSTRAINT [PK_Pupil] PRIMARY KEY (Id)
)
GO

CREATE TABLE Teacher_Pupil 
(
TeacherId int,
PupilId int,
CONSTRAINT PK_Teacher_Pupil PRIMARY KEY(TeacherId, PupilId),
CONSTRAINT FK_Teacher 
  FOREIGN KEY (TeacherId) REFERENCES Teacher (Id),
CONSTRAINT FK_Pupil
 FOREIGN KEY (PupilId) REFERENCES Pupil (Id)
)
GO

SELECT t.* FROM Teacher_Pupil tp
JOIN Teacher t ON t.Id = tp.TeacherId
JOIN Pupil p ON tp.PupilId = p.Id
WHERE p.FirstName = N'გიორგი'
GO