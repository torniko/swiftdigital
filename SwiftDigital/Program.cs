﻿using System;

namespace SwiftDigital
{
    class Program
    {
        
        static void Main(string[] args)
        {
            // #1 პალინდრომის შემოწმება. მუშაობს ასევე ტექსტზე, გამორიცხავს ყველაფერს გარდა ასოებისა.
            Console.WriteLine(Palindrome.IsPalindrome("madam"));

            // #2 დახურდავება
            Console.WriteLine(_2_MinSplit.minSplit(11));

            // #3 მინიმალური დადებითი მთელი რომელიც არ შედის ერეიში.
            Console.WriteLine(_3_NotContains.notContains(new int[]{-1, -2, 0, 1, 1, 2, 3, 4, 5}));

            // #4 არის თუ არა ფრჩხილები მათმატიკურად სწორად დასმული
            Console.WriteLine(_4_BooleanIsProperly.isProperly("((()())"));

            // #5 რამდენი სხვადასხვანაირი კომბინაციით ავალთ მოცემულ სართულზე თუ 1 ან 2 ს გადავაბიჯებთ კიბეს
            Console.WriteLine(_5_CountVariants.countVariants(12));

            // #6 O(1) ში აითემის წაშლა.
            var dt = new DeleteDataStructure();
            dt.Insert(3);
            dt.Insert(4);
            dt.Insert(5);

            dt.Remove(4);
            Console.WriteLine(string.Join(',', dt.Nums));

            // #7 დავალების ორივე ტასკი, ატაჩმენტზეა მეილზე. SQL.

            // #8 RSS ვალუტის კონვერტერი

            var exchangeRate = RSSCurrencyConverter.Convert("EUR", "USD").Result;

            Console.WriteLine(exchangeRate);


        }
    }
}
