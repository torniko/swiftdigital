﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace SwiftDigital
{
    public class RSSCurrencyConverter
    {
        static readonly HttpClient client = new HttpClient();

        public static async Task<decimal> Convert(string from, string to)
        {
            var response = await client.GetAsync("http://www.nbg.ge/rss.php");
            response.EnsureSuccessStatusCode();
            var xml = await response.Content.ReadAsStringAsync();

            var description = GetDescription(xml);

            var currencies = GetCurrencies(description);

            var rateFrom = currencies.Single(m => m.Code == from).Rate;
            var rateTo = currencies.Single(m => m.Code == to).Rate;

            return Math.Round(rateFrom / rateTo, 4);
        }

        private static string GetDescription(string xml)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var description = xmlDoc.SelectSingleNode("rss/channel/item/description").InnerText;

            return description;
        }

        private static List<Currency> GetCurrencies(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var currencies = doc.DocumentNode
                .Descendants("tr")
                .Where(tr => tr.Elements("td").Count() > 1)
                .Select(tr => new Currency
                {
                    Code = tr.Elements("td").First().InnerText,
                    Quantity = int.Parse(tr.Elements("td").Skip(1).First().InnerText.Split(' ').First()),
                    Rate = decimal.Parse(tr.Elements("td").Skip(2).First().InnerText)
                })
               .ToList();

            return currencies;
        }

        class Currency
        {
            public string Code { get; set; }

            public int Quantity { get; set; }

            private decimal rate;
            public decimal Rate
            {
                get { return rate; }
                set
                {
                    rate = value / Quantity;
                }
            }
        }
    }
}
