﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwiftDigital
{
    public class _3_NotContains
    {
        public static int notContains(int[] array)
        {
            HashSet<int> nums1 = new HashSet<int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 0)
                {
                    nums1.Add(array[i]);
                }

            };
            int[] nums2 = new int[nums1.Count];
            nums1.CopyTo(nums2);
            array = nums2;
            Array.Sort(array);

            int k = 1;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 0)
                {
                    if (array[i] == k)
                    {
                        k++;
                    }
                    else
                    {
                        return k;
                    }
                }
            }
            return k;
        }
    }
}



