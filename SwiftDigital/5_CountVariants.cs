﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwiftDigital
{
    public class _5_CountVariants
    {

        public static Int64 countVariants(int stearsCount)
        {
            if (stearsCount < 3)
                return stearsCount;

            Int64 f2 = 2;
            Int64 f1 = 1;
            Int64 floor = 0;

            int i = 3;
            while (i++ <= stearsCount)
            {
                floor = f2 + f1;
                f1 = f2;
                f2 = floor;
            }

            return floor;
        }
    }
}
