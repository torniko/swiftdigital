﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwiftDigital
{
    public class _2_MinSplit
    {
        public static int minSplit(int amount)
        {
            var xurdebi = new int[] { 50, 20, 10, 5, 1 };

            int count = 0;

            for (int i = 0; i < xurdebi.Length; i++)
            {
                if (xurdebi[i] <= amount)
                {
                    count = count + amount / xurdebi[i];
                    amount = amount % xurdebi[i];
                }
            }
            return count;
        }
    }
}
