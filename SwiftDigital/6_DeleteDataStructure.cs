﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwiftDigital
{
    public class DeleteDataStructure
    {
        public List<int> Nums { get; }
        private Dictionary<int, int> map;

        public DeleteDataStructure()
        {
            map = new Dictionary<int, int>();
            Nums = new List<int>();
        }

        public bool Insert(int val)
        {
            if (map.ContainsKey(val))
            {
                return false;
            }

            Nums.Add(val);
            map.Add(val, Nums.Count - 1);
            return true;
        }

        public bool Remove(int val)
        {
            if (!map.ContainsKey(val))
            {
                return false;
            }
            int i = map[val];
            Nums[i] = Nums[Nums.Count - 1];
            map[Nums[i]] = i;

            Nums.RemoveAt(Nums.Count - 1);
            map.Remove(val);

            return true;
        }


    }
}
